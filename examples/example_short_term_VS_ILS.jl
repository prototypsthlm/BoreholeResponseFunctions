using BoreholeResponseFunctions
using Parameters
using Plots

#Define default parameters for the simulation
props = Para()
@unpack λg, Rb = props

st = 10 .^range(log10(10.), stop=log10(3600*1e5), length=200)
Tst  = [gClaessonJaved(t,props)  for t in st]
gst   = [(2pi*λg)*(gClaessonJaved(t,props))  for t in st]  

Tils  = [1/(2pi*λg) * ILS(t, props) + Rb for t in st]
gils  = [ILS(t, props) + props.Rb * (2pi*λg ) for t in st]


p0  = plot(log10.(st), [gst gils], label = ["gCJ" "gils"], xticks = 1:1:8, xlabel = "log10(t)")
p01 = plot(log10.(st), log10.(gils-gst), label = "log10(gils - gCJ)", xticks = 1:1:8 , xlabel = "log10(t)")
p02 = plot(p0,p01,layout = (2,1))


# filename = joinpath(joinpath(splitdir(abspath(dirname(@__FILE__)))[1]), "assets","comparison_gClaessonJaved_VS_ILS")
# savefig(p02,filename)


