using BoreholeResponseFunctions
using Test

# test 1 for the function seg2seg_numb: for the given condition the problem is "equivalent" to the ILS problem

# Philippe, M., Bernier, M., Marchio, D., 2009. Validity ranges of three analytical solutions to heat transfer
# in the vicinity of single boreholes. Geothermics 38, 407–413.


H = 100.; #m
r = 0.05 #m
α = 0.53*1e-6 #m2s−1
time = 3600. #s

res1 = seg2seg_numb(time, α, r, 0., H);
res2 = ILS(time, r, α)

@test abs(res1 - res2) / res2 <= 1e-2


# test for the function seg2seg(t::Array{T,1}, αg::T, δr::T, δz::T, H::T)::Array{T,1} where {T<:AbstractFloat}.
# It should give the same results as the function seg2seg_numb

H = 100.; #m
r = 20. #m
α = 0.53*1e-6 #m2s−1
time = 3600*8760. #s


res1 = seg2seg_numb(time, α, r, 0., H);
res2 = seg2seg(time, α, r, 0., H);

@test res1≈res2


# test for seg2seg(t::Array{T,1}, props::Para, δr::T, δz::T, H::T)::Array{T,1} where {T<:AbstractFloat}.
# It tests if calculating the integrals for each single time step with the function is the same as calculating them singularly

H = 100.;  #m
r = 0.05;  #m
pp=Para();
z=0.;      #m
time = collect(3600*24*365.:3600*24*365.:3600*24*365*100.); #s

res1 = seg2seg(time, pp, r, z, H);
res2 = fill(0., length(time));
for ii in 1:length(time)
    res2[ii] = seg2seg(time[ii], pp, r, z, H);
end

@test res1≈res2

# test for the function seg2seg(t::Array{T,1}, αg::T, δr::T, δz::T, H::T)::Array{T,1} where {T<:AbstractFloat}
# It tests if calculating the integrals for each single time step with the function is the same as calculating them singularly

H = 100.;  #m
r = 0.05   #m
α = 0.53*1e-6 #m2s−1
z=0.       #m
time = collect(3600*24*365.:3600*24*365.:3600*24*365*100.) #s

res1 = seg2seg(time, α, r, z, H);
res2 = fill(0., length(time))
for ii in 1:length(time)
    res2[ii] = seg2seg(time[ii], α, r, z, H)
end

@test res1≈res2
